const express = require('express');
const path = require('path');

const server = express();

const PORT = 3000;

/*=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

server.use(express.static(__dirname + '/public', {
    index: false
}));

server.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'))
})

server.get('*', (req, res) => {
    res.redirect('/');
})

server.listen(PORT, () => {
    console.log('Server listening on port ' + PORT);
})