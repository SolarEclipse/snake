/// <reference path="./p5/p5.global-mode.d.ts" />
/*=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-==-=-=-=-=-=-=*/

let snake;
let grid = 20;
let food;

function setup() {
    createCanvas(400, 400);
    snake = new Snake();
    frameRate(8);
    genFood();
}

function draw() {
    background(50);

    snake.isDead();
    snake.update();
    snake.show();

    if (snake.eat(food)) {
        genFood();
    }

    fill(200, 0, 0);
    rect(food.x, food.y, grid, grid);
}

function genFood() {
    let cols = floor(width / grid);
    let rows = floor(height / grid);

    food = createVector(
        floor(random(cols)),
        floor(random(rows))
    );

    food.mult(grid);

}

function keyPressed() {
    if (keyCode === UP_ARROW) {
        snake.direction(0, -1);
    } else if (keyCode === DOWN_ARROW) {
        snake.direction(0, 1)
    } else if (keyCode === LEFT_ARROW) {
        snake.direction(-1, 0)
    } else if (keyCode === RIGHT_ARROW) {
        snake.direction(1, 0)
    }
}