/// <reference path="./p5/p5.global-mode.d.ts" />
/*=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-==-=-=-=-=-=-=*/

class Snake {
    constructor() {
        this.x = 0;
        this.y = 0;

        this.xspeed = 1;
        this.yspeed = 0;

        this.tail = [];
        this.segements = 0;
    }

    update() {
        if (this.segements === this.tail.length) {
            for (let i = 0; i < this.tail.length; i++) {
                this.tail[i] = this.tail[i + 1];
            }
        }
        this.tail[this.segements - 1] = createVector(
            this.x,
            this.y
        )


        this.x = this.x + this.xspeed * grid;
        this.y = this.y + this.yspeed * grid;

        this.checkBorder();
    }

    show() {
        fill(255);
        for (let i = 0; i < this.tail.length; i++) {
            rect(this.tail[i].x, this.tail[i].y, grid, grid);
        }

        fill(255);
        rect(this.x, this.y, grid, grid);
    }

    direction(x, y) {
        this.xspeed = x;
        this.yspeed = y;
    }

    isDead() {
        for (let i = 0; i < this.tail.length; i++) {
            let d = dist(this.x, this.y, this.tail[i].x, this.tail[i].y)

            if (d <= 0) {
                console.log('GAME OVER');
                this.segements = 0;
                this.tail = [];
            }
        }
    }

    checkBorder() {
        if (this.x >= width) {
            this.x = 0
        } else if (this.x < 0) {
            this.x = width - grid;
        } else if (this.y >= height) {
            this.y = 0;
        } else if (this.y < 0) {
            this.y = height - grid;
        }

    }

    eat() {
        if (this.x == food.x && this.y == food.y) {
            this.segements++;
            return true;
        } else {
            return false;
        }
    }





}